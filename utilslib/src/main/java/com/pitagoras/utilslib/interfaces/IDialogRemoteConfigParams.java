package com.pitagoras.utilslib.interfaces;

public interface IDialogRemoteConfigParams {

    String isShowDialogKey();

    String getDialogMessageKey();

    String getDialogAcceptTextKey();
}