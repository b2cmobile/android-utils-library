package com.pitagoras.utilslib.interfaces

interface WhatsNewActionsCallback {

    fun onRateClicked(sendAnalyticsOnly: Boolean)

    fun onShareClicked(sendAnalyticsOnly: Boolean)

    fun onPopupShown()

    fun onPopupClosed()
}