package com.pitagoras.utilslib.interfaces

interface IAccessibilityHintAppearanceParams {

    /**
     * callback method to provide app icon resource for hint
     */
    fun getAppIconResId(): Int

    /**
     * callback method to provide hint gif resource
     */
    fun getHintGifResId(): Int

    /**
     * callback method to provide title for hint
     */
    fun getHintTitleTextResId(): Int
}