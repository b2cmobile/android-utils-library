package com.pitagoras.utilslib.utils

import android.graphics.Color
import android.support.v4.graphics.ColorUtils
import android.text.TextUtils
import android.view.View
import android.widget.HorizontalScrollView

import com.pitagoras.utilslib.ExceptionLogger

import java.util.LinkedHashSet


const val DEF_COLORS = "#e51c23,#e91e63,#9c27b0,#673ab7,#3f51b5,#5677fc,#03a9f4," +
        "#00bcd4,#009688,#259b24,#8bc34a,#cddc39,#ffeb3b,#ffc107," +
        "#ff9800,#ff5722,#795548,#9e9e9e,#607d8b,#ffffff"
const val DEF_COUNT_SHADE_COLORS = 10

private const val LUMINANCE_LIMIT = 0.75f

fun getShadesForColor(aColor: Int, shadesCount: Int): MutableSet<Int> {
    val shadeColors = LinkedHashSet<Int>()
    for (i in shadesCount downTo 1) {
        val hsvFormat = FloatArray(3)
        try {
            Color.colorToHSV(aColor, hsvFormat)
            hsvFormat[1] *= i / shadesCount.toFloat() // changing saturation only
            shadeColors.add(Color.HSVToColor(hsvFormat))
        } catch (e: Exception) {
            ExceptionLogger.logException(e)
        }
    }
    return shadeColors
}

fun getMainColors(listOfColors: String): MutableSet<Int> {
    if (TextUtils.isEmpty(listOfColors)) {
        ExceptionLogger.logException(Exception("Passed list of colors is empty"))
        return getMainColors(DEF_COLORS)
    }
    val colorSet = LinkedHashSet<Int>()
    val colorArray = listOfColors.split(",")
    for (color in colorArray) {
        try {
            colorSet.add(Color.parseColor(color))
        } catch (e: Exception) {
            ExceptionLogger.logException(Exception("Color has wrong format: " + color + ", " + e.message))
            if (listOfColors != DEF_COLORS) {
                return getMainColors(DEF_COLORS) // if passed list of colors has wrong format of color, default colors will be used
            }
        }
    }
    if (colorSet.isEmpty()) {
        return getMainColors(DEF_COLORS)
    }
    return colorSet
}

/**
 * Scroll to specific view in horizontal scrollview, so specified view will be in the middle.
 * If passed view or scrollview are null, nothing will happen.
 *
 * @param scrollView horizontal scroll view, which should perform scroll to passed view.
 * @param view to which we need to scroll
 */
fun focusOnView(scrollView: HorizontalScrollView?, view: View?) {
    if (view != null) {
        scrollView?.smoothScrollTo((view.left + view.right - scrollView.width) / 2, 0)
    }
}

fun isColorDark(color: Int) = ColorUtils.calculateLuminance(color) < LUMINANCE_LIMIT