package com.pitagoras.utilslib.dialogs

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.pitagoras.utilslib.ExceptionLogger
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.UtilsDialogManager
import com.pitagoras.utilslib.models.WhatsNewRemoteConfigKeys

class ClearBrowsingHistoryDialogFragment : DialogFragment(), View.OnClickListener {

    private lateinit var raysRotateAnim: Animation
    private lateinit var raysImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        raysRotateAnim = AnimationUtils.loadAnimation(activity, R.anim.rays_rotate_anim)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.clear_browser_history_layout, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val transparentDialog = super.onCreateDialog(savedInstanceState)
        transparentDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        transparentDialog.setCanceledOnTouchOutside(false)
        return transparentDialog
    }

    override fun onResume() {
        super.onResume()
        raysImageView.startAnimation(raysRotateAnim)
    }

    override fun onClick(v: View?) {
        try {
            when (v?.id) {
                R.id.buttonGotIt -> {
                    UtilsDialogManager.getClearBrowserPopupAnalyticsCallback()?.onGotItClicked()
                    dismissAllowingStateLoss()
                }

                R.id.buttonDialogClose -> {
                    UtilsDialogManager.getClearBrowserPopupAnalyticsCallback()?.onClearBrowserPopupClosed()
                    dismissAllowingStateLoss()
                }
            }
        } catch (ex: Exception) {
            ExceptionLogger.logException(Exception("IClearBrowserPopupCallback hasn't been initialized. Activity/Fragment should implement IClearBrowserPopupCallback"))
            dismissAllowingStateLoss()
        }
    }

    override fun onPause() {
        super.onPause()
        raysImageView.clearAnimation()
    }


    /**
     * returns the associated resource identifier. Returns 0 if no such resource was found. (0 is not a valid resource ID.)
     **/
    private fun getIconResourceId(position: Int): Int {
        return resources.getIdentifier("ic_whats_new_section_" + (position + 1), "drawable", activity?.packageName)
    }

    private fun initViews(view: View) {
        raysImageView = view.findViewById(R.id.imageViewWhatsNewRays)
        val titleTextView: TextView = view.findViewById(R.id.textVieClearBrowserTitle)
        val gotItButton: Button = view.findViewById(R.id.buttonGotIt)
        view.findViewById<ImageButton>(R.id.buttonDialogClose).setOnClickListener(this)
        //ViewConfigHelper.setTextViewTextColor(titleTextView, WhatsNewRemoteConfigKeys.POPUP_TITLE_TEXT_COLOR.paramName)
        setupButton(gotItButton, WhatsNewRemoteConfigKeys.POPUP_RATE_TEXT.paramName)
    }

    private fun setupButton(button: Button, configParamName: String) {
        //@TODO reenable this remote config vals
        //ViewConfigHelper.setButtonText(button, configParamName, Locale.getDefault().displayLanguage)
        //ViewConfigHelper.setButtonTextColor(button, WhatsNewRemoteConfigKeys.POPUP_BUTTON_TEXT_COLOR.paramName)
        //ViewConfigHelper.setButtonBackgroundColor(button, WhatsNewRemoteConfigKeys.POPUP_BUTTON_COLOR.paramName)
        button.setOnClickListener(this)
    }


}