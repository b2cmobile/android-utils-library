package com.pitagoras.utilslib.dialogs;

import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pitagoras.utilslib.R;
import com.pitagoras.utilslib.interfaces.IDialogAnalytics;
import com.pitagoras.utilslib.utils.PolicyMovementMethod;

public class DefaultDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String KEY_PREF_USER_ACCEPTED = "user_accepted_pp";

    private static final String TAG = DefaultDialogFragment.class.getSimpleName();
    private static final String BUNDLE_KEY_ICON_RES_ID = "icon_res_id";
    private static final String BUNDLE_KEY_DIALOG_MESSAGE = "dialog_message";
    private static final String BUNDLE_KEY_BUTTON_TEXT = "button_text";

    private IDialogAnalytics mAnalyticsCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mAnalyticsCallback != null) {
            mAnalyticsCallback.onDialogAppeared();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_layout, container);
        setupView(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .edit()
                .putBoolean(KEY_PREF_USER_ACCEPTED, true)
                .apply();

        if (mAnalyticsCallback != null) {
            mAnalyticsCallback.onUserAccepted();
        }
        dismissAllowingStateLoss();
    }

    public void setAnalyticsCallback(IDialogAnalytics aAnaliticsCallback) {
        mAnalyticsCallback = aAnaliticsCallback;
    }

    private void setupView(View aView) {
        ImageView iconImageView = aView.findViewById(R.id.iv_icon);
        TextView messageTextView = aView.findViewById(R.id.tv_message);
        Button acceptButton = aView.findViewById(R.id.btn_accept);
        Bundle args = getArguments();
        if (args != null) {
            int iconResId = args.getInt(BUNDLE_KEY_ICON_RES_ID);
            //0 is the default value, iconResId was not set
            if (iconResId != 0) {
                iconImageView.setImageResource(iconResId);
            }
            //if values were not set on RC use default values
            String text = args.getString(BUNDLE_KEY_DIALOG_MESSAGE);
            messageTextView.setMovementMethod(new PolicyMovementMethod(mAnalyticsCallback));
            if (TextUtils.isEmpty(text)) {
                ApplicationInfo applicationInfo = getActivity().getApplicationInfo();
                int stringId = applicationInfo.labelRes;
                text = String.format(getString(R.string.pp_message),
                        stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : getActivity()
                                .getString(stringId), getString(R.string.pp_link));
            } else {
                text = TextUtils.concat(text, " ", getString(R.string.pp_link)).toString();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                messageTextView.setText(Html.fromHtml(text, 0));
            } else {
                messageTextView.setText(Html.fromHtml(text));
            }
            text = args.getString(BUNDLE_KEY_BUTTON_TEXT, getString(R.string.pp_accept_text));
            acceptButton.setText(text);
            acceptButton.setOnClickListener(this);
        } else {
            Log.d(TAG, getString(R.string.error_lib_setup));
        }
    }

    public static class Builder {
        private int mThemeResId;
        private int mIconResId;
        private String mDialogMessage;
        private String mAcceptButtonText;

        public Builder setThemeResId(int aThemeResId) {
            this.mThemeResId = aThemeResId;
            return this;
        }

        public Builder setIconResId(int aIconResId) {
            this.mIconResId = aIconResId;
            return this;
        }

        public Builder setDialogMessage(String aDialogMessage) {
            this.mDialogMessage = aDialogMessage;
            return this;
        }

        public Builder setAcceptButtonText(String aAcceptButtonText) {
            this.mAcceptButtonText = aAcceptButtonText;
            return this;
        }

        public DefaultDialogFragment create() {
            DefaultDialogFragment fragment = new DefaultDialogFragment();
            Bundle args = new Bundle();
            args.putInt(BUNDLE_KEY_ICON_RES_ID, mIconResId);
            args.putString(BUNDLE_KEY_DIALOG_MESSAGE, mDialogMessage);
            args.putString(BUNDLE_KEY_BUTTON_TEXT, mAcceptButtonText);
            fragment.setArguments(args);
            fragment.setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, mThemeResId);
            return fragment;
        }
    }
}
