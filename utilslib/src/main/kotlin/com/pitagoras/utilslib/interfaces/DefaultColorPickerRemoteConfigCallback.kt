package com.pitagoras.utilslib.interfaces

internal class DefaultColorPickerRemoteConfigCallback : IColorPickerRemoteConfig {

    override val shadesCountKey: String
        get() = COLOR_PICKER_COUNT_OF_SHADE_COLORS

    override val mainColorsKey: String
        get() = COLOR_PICKER_LIST_OF_COLORS

    override val colorsTitleTextKey: String
        get() = COLOR_PICKIER_TITLE_TEXT

    override val colorsTitleTextColorKey: String
        get() = COLOR_PICKIER_TITLE_TEXT_COLOR

    override val colorsTitleTextSizeKey: String
        get() = COLOR_PICKIER_TITLE_TEXT_SIZE

    override val shadesTitleTextKey: String
        get() = COLOR_PICKIER_SUB_TITLE_TEXT

    override val shadesTitleTextColorKey: String
        get() = COLOR_PICKIER_SUB_TITLE_TEXT_COLOR

    override val shadesTitleTextSizeKey: String
        get() = COLOR_PICKIER_SUB_TITLE_TEXT_SIZE


    companion object {
        private const val COLOR_PICKIER_TITLE_TEXT = "app_colors_title_text"
        private const val COLOR_PICKIER_TITLE_TEXT_SIZE = "app_colors_title_text_size"
        private const val COLOR_PICKIER_TITLE_TEXT_COLOR = "app_colors_title_text_color"
        private const val COLOR_PICKIER_SUB_TITLE_TEXT = "app_colors_sub_title_text"
        private const val COLOR_PICKIER_SUB_TITLE_TEXT_SIZE = "app_colors_sub_title_text_size"
        private const val COLOR_PICKIER_SUB_TITLE_TEXT_COLOR = "app_colors_sub_title_text_color"
        private const val COLOR_PICKER_LIST_OF_COLORS = "colors_screen_list_of_colors"
        private const val COLOR_PICKER_COUNT_OF_SHADE_COLORS = "colors_screen_count_of_shade_colors"
    }

}
