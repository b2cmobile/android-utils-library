package com.pitagoras.utilslib_demo.callbacks;

import android.support.annotation.NonNull;
import android.util.Log;

import com.pitagoras.utilslib.interfaces.IClearBrowserPopupAnalyticsCallback;

public class ClearBrowserPopupAnalyticCallback implements IClearBrowserPopupAnalyticsCallback {

    private static final String TAG = ClearBrowserPopupAnalyticCallback.class.getSimpleName();

    @Override
    public void onGotItClicked() {
        Log.d(TAG, "onGotItClicked");
    }

    @Override
    public void onClearBrowserPopupShown() {
        Log.d(TAG, "onClearBrowserPopupShown");
    }

    @Override
    public void onClearBrowserPopupClosed() {
        Log.d(TAG, "onClearBrowserPopupClosed");
    }

    @Override
    public void onAccessibilityStatusChanged(@NonNull String category, @NonNull String action, @NonNull String label) {
        Log.d(TAG, "onAccessibilityStatusChanged: " + category + " " + action + " " + label);
    }

    @Override
    public void onBrowserHistoryServerRequestSent(int countOfUrls) {
        Log.d(TAG, "onBrowserHistoryServerRequestSent: " + countOfUrls);
    }
}
