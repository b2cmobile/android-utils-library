package com.pitagoras.utilslib_demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pitagoras.utilslib.UtilsNotificationHelper;

public class NotificationAlarmReceiver extends BroadcastReceiver {

    private static final String TAG = NotificationAlarmReceiver.class.getSimpleName();
    public static final String ACCESSIBILITY_NOTIFICATION_ALARM = "ACCESSIBILITY_NOTIFICATION_ALARM";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            Log.d(TAG, "receiver intent is null");
        } else if (ACCESSIBILITY_NOTIFICATION_ALARM.equals(intent.getAction())) {
            Log.d(TAG, "accessibility reminder notification received");
            UtilsNotificationHelper.showAccessibilityNotification(context, getNotifActionIntent(context), "Your device needs some cleaning/boosting");
        } else if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.d(TAG, "boot completed received");
            UtilsNotificationHelper.Companion.setAccessibilityNotificationAlarm(context, NotificationAlarmReceiver.class, ACCESSIBILITY_NOTIFICATION_ALARM ,false);
        }
    }

    private Intent getNotifActionIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}

