package com.pitagoras.utilslib.views

import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.TextView

import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import com.pitagoras.remoteconfigsdk.ViewConfigHelper
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.ExceptionLogger
import com.pitagoras.utilslib.interfaces.*
import com.pitagoras.utilslib.utils.*

import java.util.ArrayList

class ColorPickerWrapper(private val mRootView: View?,
                         private val mColorPickerWrapperCallback: IColorPickerWrapperCallback?) : ColorPicker.ColorChangedListener {

    private var mColorPickerAnalytics: IColorPickerAnalytics? = null
    private var mColorPickerRemoteConfig: IColorPickerRemoteConfig? = null
    private var mCachedColorSelectionCallback: ICachedColorSelectionCallback? = null

    private lateinit var mColorShadePicker: ColorPicker
    private lateinit var mMainColorPicker: ColorPicker

    private var mSelectColorTextView: TextView? = null
    private var mSelectShadeTextView: TextView? = null
    private var mHorizontalScrollView: HorizontalScrollView? = null

    private var mItemSelectionBorderWidthPx = 1 // 1 pixel width by default
    private var mItemSelectionBorderColor = 0 // Transparent border by default

    private var mColorSelectionCounter: Int = 0
    private var mUseDefaultColorAtFirstLaunch: Boolean = false

    fun setColorPickerAnalytics(colorPickerAnalytics: IColorPickerAnalytics) {
        mColorPickerAnalytics = colorPickerAnalytics
    }

    /**
     * If the callback will be not set, the default class DefaultColorPickerRemoteConfigCallback will be used.
     */
    fun setColorPickerRemoteConfig(colorPickerRemoteConfig: IColorPickerRemoteConfig) {
        mColorPickerRemoteConfig = colorPickerRemoteConfig
    }

    /**
     * If set to true and previously there was no selected color, the first color from list will be selected
     */
    fun setUseDefaultColorAtFirstLaunch(useDefaultColorAtFirstLaunch: Boolean) {
        mUseDefaultColorAtFirstLaunch = useDefaultColorAtFirstLaunch
    }

    /**
     * If the callback will be not set, the default class DefaultColorSelectionCallback will be used.
     */
    fun setCachedColorSelectionCallback(cachedColorSelectionCallback: ICachedColorSelectionCallback) {
        mCachedColorSelectionCallback = cachedColorSelectionCallback
    }

    val colorShadePicker get() = mColorShadePicker

    val mainColorPicker get() = mMainColorPicker

    val rootView get() = mRootView

    fun init() {
        if (mRootView == null) {
            ExceptionLogger.logException(IllegalStateException("Root view is null"))
            return
        }

        mSelectColorTextView = mRootView.findViewById(R.id.selectColorTextView)
        mSelectShadeTextView = mRootView.findViewById(R.id.selectShadeTextView)

        mMainColorPicker = mRootView.findViewById(R.id.colorPicker)
        mMainColorPicker.setColorSelectionBorder(mItemSelectionBorderWidthPx, mItemSelectionBorderColor)
        mMainColorPicker.setColorChangedListener(this)

        mColorShadePicker = mRootView.findViewById(R.id.colorShadePicker)
        mColorShadePicker.setColorSelectionBorder(mItemSelectionBorderWidthPx, mItemSelectionBorderColor)
        mColorShadePicker.setColorChangedListener(this)

        if (mColorPickerRemoteConfig == null) {
            mColorPickerRemoteConfig = DefaultColorPickerRemoteConfigCallback()
        }
        configRemoteViews()

        mHorizontalScrollView = mRootView.findViewById(R.id.shadeColorsHorizontalScrollView)

        if (mCachedColorSelectionCallback == null) {
            mCachedColorSelectionCallback = DefaultColorSelectionCallback(mRootView.context)
        }

        // Set previously selected main color or use first in the list as default.
        if (mCachedColorSelectionCallback!!.isMainColorSpecified || mUseDefaultColorAtFirstLaunch) {
            val selectedMainColor = mCachedColorSelectionCallback?.getLastSpecifiedMainColor(mMainColorPicker.colors.iterator().next()) // set first color as selected
            if (selectedMainColor != null) {
                mMainColorPicker.selectedColor = selectedMainColor
                updateShadesWithSpecificColor(selectedMainColor)
            }

            // If color was previously selected than set it again.
            // And if no and we need to set a default color - get the first color from the list of shades.
            if (mCachedColorSelectionCallback!!.isColorSpecified || mUseDefaultColorAtFirstLaunch) {
                val previouslySpecifiedColor = mCachedColorSelectionCallback!!.getLastSpecifiedColor(mColorShadePicker.selectedColor)
                mColorShadePicker.selectedColor = previouslySpecifiedColor
                if (!mUseDefaultColorAtFirstLaunch) {
                    mColorPickerWrapperCallback?.onColorChanged(mColorShadePicker.id, previouslySpecifiedColor)
                }
                // scroll to selected color once scrollview is initialized
                mHorizontalScrollView?.post { focusOnView(mHorizontalScrollView, mColorShadePicker.selectedView) }
            }
        }
    }

    /**
     * To get most common user selection before leaving the activity, this method should be
     * called in {@link android.app.Activity#onPause}.
     * Method makes some validation and invokes analytic callback with related params.
     */
    fun trackLastSelectedColorEvent() {
        if (mColorSelectionCounter > 0 && mColorPickerAnalytics != null) {
            mColorPickerAnalytics?.onLastColorSelected(
                    ArrayList(mMainColorPicker.colors).indexOf(mMainColorPicker.selectedColor) + 1,
                    mMainColorPicker.selectedColor,
                    ArrayList(mColorShadePicker.colors).indexOf(mColorShadePicker.selectedColor) + 1,
                    mColorShadePicker.selectedColor)
            mColorSelectionCounter = 0
        }
    }

    override fun onColorChanged(colorPickerId: Int, position: Int, newColor: Int) {
        if (colorPickerId == R.id.colorPicker) {
            if (mSelectShadeTextView?.visibility != View.VISIBLE) {
                mColorPickerAnalytics?.onShadeColorsShown()
            }
            mColorPickerAnalytics?.onColorClicked(position + 1, newColor)
            updateShadesWithSpecificColor(newColor)
            mColorSelectionCounter++
            mCachedColorSelectionCallback?.setLastSpecifiedMainColor(newColor)
        } else if (colorPickerId == R.id.colorShadePicker) {
            mColorPickerAnalytics?.onShadeColorClicked(position + 1, newColor, mMainColorPicker.selectedColor)
        }
        mCachedColorSelectionCallback?.setLastSpecifiedColor(newColor)
        mColorPickerWrapperCallback?.onColorChanged(colorPickerId, newColor)
        focusOnView(mHorizontalScrollView, mColorShadePicker.selectedView)
    }

    fun setColorSelectionBorder(widthPx: Int, borderColor: Int) {
        mItemSelectionBorderWidthPx = widthPx
        mItemSelectionBorderColor = borderColor
    }

    /**
     * Set the enabled state of passed root view and all its views inside.
     */
    fun setEnabled(isEnabled: Boolean = true) {
        if (mRootView == null) {
            ExceptionLogger.logException(IllegalStateException("Root view is null"))
            return
        }
        if (!isEnabled) {
            mSelectShadeTextView?.visibility = View.INVISIBLE
            mColorShadePicker.clear()
            mMainColorPicker.selectedColor = 0
        }
        mRootView.alpha = if (isEnabled) 1.0f else 0.5f
        setEnabledView(mRootView, isEnabled)
    }

    fun resetPicker() {
        mSelectShadeTextView?.visibility = View.INVISIBLE
        mColorShadePicker.clear()
        mMainColorPicker.selectedColor = 0
    }

    private fun configRemoteViews() {
        if (mColorPickerRemoteConfig == null) {
            ExceptionLogger.logException(IllegalStateException("RemoteConfigCallback is null!"))
            // cannot config views with remote values
            // set default instead
            mMainColorPicker.initView(getMainColors(DEF_COLORS))
            mColorShadePicker.columnCount = DEF_COUNT_SHADE_COLORS
            return
        }
        ViewConfigHelper.setTextView(mSelectColorTextView,
                mColorPickerRemoteConfig!!.colorsTitleTextKey,
                mColorPickerRemoteConfig!!.colorsTitleTextSizeKey,
                mColorPickerRemoteConfig!!.colorsTitleTextColorKey)

        ViewConfigHelper.setTextView(mSelectShadeTextView,
                mColorPickerRemoteConfig!!.shadesTitleTextKey,
                mColorPickerRemoteConfig!!.shadesTitleTextSizeKey,
                mColorPickerRemoteConfig!!.shadesTitleTextColorKey)

        val colorsString = RemoteConfigHelper.getString(mColorPickerRemoteConfig!!.mainColorsKey, DEF_COLORS)
        val colors = getMainColors(colorsString)
        mMainColorPicker.initView(colors)

        mColorShadePicker.columnCount = RemoteConfigHelper.getInteger(mColorPickerRemoteConfig!!.shadesCountKey, DEF_COUNT_SHADE_COLORS)
    }

    private fun setEnabledView(view: View?, enabled: Boolean) {
        if (view != null) {
            view.isEnabled = enabled
            view.isFocusable = enabled
            if (view is ViewGroup) {
                for (i in 0 until view.childCount)
                    setEnabledView(view.getChildAt(i), enabled)
            }
        }
    }

    private fun updateShadesWithSpecificColor(color: Int) {
        val shadesCount = if (mColorPickerRemoteConfig == null)
            DEF_COUNT_SHADE_COLORS
        else
            RemoteConfigHelper.getInteger(mColorPickerRemoteConfig!!.shadesCountKey, DEF_COUNT_SHADE_COLORS)
        val shadeColors = getShadesForColor(color, shadesCount)
        mColorShadePicker.initView(shadeColors)
        mColorShadePicker.selectedColor = shadeColors.iterator().next() // set first color with shade as selected
        mSelectShadeTextView?.visibility = View.VISIBLE
    }
}
