package com.pitagoras.utilslib.interfaces

interface IColorPickerAnalytics {
    /**
     * This method will be invoked after color was selected and user leaves activity.
     * Event should be sent in order to get most common user selection before closing activity.
     * In order to send the event, method {@link com.pitagoras.utilslib.views.ColorPickerWrapper#trackLastSelectedColorEvent}
     * should be called in {@link android.app.Activity#onPause}
     *
     * @param position           position of main color in array received from RC or defaults
     * @param color              #RRGGBB format of selected color
     * @param shadeColorPosition position of shade color in array
     * @param shadeColor         #RRGGBB format of selected shade color
     */
    fun onLastColorSelected(position: Int, color: Int, shadeColorPosition: Int, shadeColor: Int)

    /**
     * Method will be invoked when shade colors become visible, after first main color was selected.
     */
    fun onShadeColorsShown()

    /**
     * @param position position of the color in array received from RC or defaults
     * @param color    #RRGGBB format of selected color
     */
    fun onColorClicked(position: Int, color: Int)

    /**
     * @param position   position of shade color in array
     * @param mainColor  #RRGGBB format of main selected color, for which shade will be selected
     * @param shadeColor #RRGGBB format of selected shade color
     */
    fun onShadeColorClicked(position: Int, mainColor: Int, shadeColor: Int)
}
