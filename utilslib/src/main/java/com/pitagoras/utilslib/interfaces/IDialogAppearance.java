package com.pitagoras.utilslib.interfaces;

public interface IDialogAppearance {

    int getDialogThemeResId();

    int getDialogIconResId();
}