package com.pitagoras.utilslib

import com.pitagoras.utilslib.interfaces.IExceptionLogger

object ExceptionLogger {
    var exceptionLogger: IExceptionLogger? = null

    fun logException(exception: Exception) {
        exceptionLogger?.logException(exception)
    }
}
