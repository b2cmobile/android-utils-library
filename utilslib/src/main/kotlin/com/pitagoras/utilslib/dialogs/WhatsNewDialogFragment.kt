package com.pitagoras.utilslib.dialogs

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import com.pitagoras.remoteconfigsdk.ViewConfigHelper
import com.pitagoras.utilslib.ExceptionLogger
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.adapters.WhatsNewAdapter
import com.pitagoras.utilslib.interfaces.WhatsNewActionsCallback
import com.pitagoras.utilslib.models.WhatsNewItemModel
import com.pitagoras.utilslib.models.WhatsNewRemoteConfigKeys
import java.util.*

class WhatsNewDialogFragment : DialogFragment(), View.OnClickListener {

    companion object {
        const val KEY_PREF_POPUP_WAS_SHOWN = "key_pref_whats_new_dialog_was_shown"
        private const val BUNDLE_KEY_PARENT_FRAGMENT_TAG = "bundle_key_fragment_tag"

        fun newInstance(fragmentTag: String?): WhatsNewDialogFragment {
            val args = Bundle()
            args.putString(BUNDLE_KEY_PARENT_FRAGMENT_TAG, fragmentTag)
            val fragment = WhatsNewDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var whatsNewAdapter: WhatsNewAdapter
    private lateinit var raysRotateAnim: Animation
    private lateinit var raysImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whatsNewAdapter = WhatsNewAdapter(activity as Context, createItemsList())
        raysRotateAnim = AnimationUtils.loadAnimation(activity, R.anim.rays_rotate_anim)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.whats_new_dialog_layout, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val transparentDialog = super.onCreateDialog(savedInstanceState)
        transparentDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        transparentDialog.setCanceledOnTouchOutside(false)
        return transparentDialog
    }

    override fun onResume() {
        super.onResume()
        raysImageView.startAnimation(raysRotateAnim)
    }

    override fun onClick(v: View?) {
        try {
            val fragmentTag: String? = arguments?.getString(BUNDLE_KEY_PARENT_FRAGMENT_TAG)
            val listener: WhatsNewActionsCallback?
            listener = if (TextUtils.isEmpty(fragmentTag)) {
                this.activity as WhatsNewActionsCallback?
            } else {
                (this.activity?.supportFragmentManager?.findFragmentByTag(fragmentTag)) as WhatsNewActionsCallback?
            }
            val buttonLink: String?
            when (v?.id) {
                R.id.buttonWhatsNewRate -> {
                    buttonLink = RemoteConfigHelper.getString(WhatsNewRemoteConfigKeys.POPUP_RATE_LINK.paramName, null)
                    listener?.onRateClicked(followTheLink(buttonLink))
                    dismissAllowingStateLoss()
                }
                R.id.buttonWhatsNewShare -> {
                    buttonLink = RemoteConfigHelper.getString(WhatsNewRemoteConfigKeys.POPUP_SHARE_LINK.paramName, null)
                    listener?.onShareClicked(followTheLink(buttonLink))
                    dismissAllowingStateLoss()
                }
                R.id.buttonWhatsNewClose -> {
                    listener?.onPopupClosed()
                    dismissAllowingStateLoss()
                }
            }
        } catch (ex: Exception) {
            ExceptionLogger.logException(Exception("WhatsNewActionsCallback hasn't been initialized. Activity/Fragment should implement WhatsNewActionsCallback"))
            dismissAllowingStateLoss()
        }
    }

    override fun onPause() {
        super.onPause()
        raysImageView.clearAnimation()
    }

    /**
     * get overridden value from specific app resources and fill the list
     **/
    private fun createItemsList(): ArrayList<WhatsNewItemModel> {
        val itemsCount = resources.getInteger(R.integer.whats_new_items_count)
        val titles = resources.getStringArray(R.array.whats_new_section_titles)
        val descriptions = resources.getStringArray(R.array.whats_new_section_descriptions)
        val itemsList = arrayListOf<WhatsNewItemModel>()
        for (i in 0..(itemsCount - 1)) {
            itemsList.add(WhatsNewItemModel(getIconResourceId(i), titles[i], descriptions[i]))
        }
        return itemsList
    }

    /**
     * returns the associated resource identifier. Returns 0 if no such resource was found. (0 is not a valid resource ID.)
     **/
    private fun getIconResourceId(position: Int): Int {
        return resources.getIdentifier("ic_whats_new_section_" + (position + 1), "drawable", activity?.packageName)
    }

    private fun initViews(view: View) {
        raysImageView = view.findViewById(R.id.imageViewWhatsNewRays)
        val titleTextView: TextView = view.findViewById(R.id.textViewWhatsNewTitle)
        val rateButton: Button = view.findViewById(R.id.buttonWhatsNewRate)
        val shareButton: Button = view.findViewById(R.id.buttonWhatsNewShare)
        val itemsListView: ExpandableListView = view.findViewById(R.id.listViewWhatsNew)
        view.findViewById<ImageButton>(R.id.buttonWhatsNewClose).setOnClickListener(this)
        ViewConfigHelper.setTextViewTextColor(titleTextView, WhatsNewRemoteConfigKeys.POPUP_TITLE_TEXT_COLOR.paramName)
        setupButton(rateButton, WhatsNewRemoteConfigKeys.POPUP_RATE_TEXT.paramName)
        setupButton(shareButton, WhatsNewRemoteConfigKeys.POPUP_SHARE_TEXT.paramName)
        itemsListView.setAdapter(whatsNewAdapter)
    }

    private fun setupButton(button: Button, configParamName: String) {
        ViewConfigHelper.setButtonText(button, configParamName, Locale.getDefault().displayLanguage)
        ViewConfigHelper.setButtonTextColor(button, WhatsNewRemoteConfigKeys.POPUP_BUTTON_TEXT_COLOR.paramName)
        ViewConfigHelper.setButtonBackgroundColor(button, WhatsNewRemoteConfigKeys.POPUP_BUTTON_COLOR.paramName)
        button.setOnClickListener(this)
    }

    /**
     * @return true if instead of handling rate/share link to follow is provided so WhatsNewActionsCallback needs to send analytics only
     */
    private fun followTheLink(buttonLink: String?): Boolean {
        if (!TextUtils.isEmpty(buttonLink)) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(buttonLink)))
            return true
        }
        return false
    }
}