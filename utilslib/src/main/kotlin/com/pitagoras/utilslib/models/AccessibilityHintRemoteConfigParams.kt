package com.pitagoras.utilslib.models

enum class AccessibilityHintRemoteConfigParams(val paramName: String) {
    ACCESSIBILITY_HINT_TITLE_TEXT("accessibility_hint_title_text"),
    ACCESSIBILITY_HINT_TITLE_TEXT_COLOR("accessibility_hint_title_text_color"),
    ACCESSIBILITY_HINT_TITLE_BACKGROUND_COLOR("accessibility_hint_title_background_color"),
    ACCESSIBILITY_HINT_BUTTON_TEXT("accessibility_hint_button_text"),
    ACCESSIBILITY_HINT_BUTTON_TEXT_COLOR("accessibility_hint_button_text_color"),
    ACCESSIBILITY_HINT_BUTTON_BACKGROUND_COLOR("accessibility_hint_button_background_color")
}