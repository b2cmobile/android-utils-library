package com.pitagoras.utilslib.interfaces

interface IColorPickerRemoteConfig {

    val shadesCountKey: String

    val mainColorsKey: String

    val colorsTitleTextKey: String
    val colorsTitleTextColorKey: String
    val colorsTitleTextSizeKey: String

    val shadesTitleTextKey: String
    val shadesTitleTextColorKey: String
    val shadesTitleTextSizeKey: String
}
