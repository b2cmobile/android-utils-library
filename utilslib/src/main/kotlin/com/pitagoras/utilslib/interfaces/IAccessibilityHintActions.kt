package com.pitagoras.utilslib.interfaces

interface IAccessibilityHintActions {

    /**
     * callback method to handle analytics
     */
    fun onGotItButtonClick()
}