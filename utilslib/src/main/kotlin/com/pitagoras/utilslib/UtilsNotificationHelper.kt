package com.pitagoras.utilslib

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.app.TaskStackBuilder
import android.util.Log
import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import com.pitagoras.remoteconfigsdk.notification.RemoteConfigNotification
import com.pitagoras.utilslib.utils.NotificationUtils
import java.text.SimpleDateFormat
import java.util.*


class UtilsNotificationHelper {

    companion object {
        private const val RC_KEY_ACCESSIBILITY_NOTIF_APPEARS_AFTER_DAYS = "accessibility_notification_appears_after_days"
        private const val RC_KEYS_ACCESSIBILITY_NOTIFICATION_PREFIX = "accessibility"

        private const val KEY_PREF_ACCESSIBILITY_NOTIFICATION_ALARM_WAS_SET = "accessibility_notif_alarm_was_set"
        private const val KEY_PREF_ACCESSIBILITY_NOTIFICATION_SHOWN = "accessibility_notification_received"
        private const val DEFAULT_ACCESSIBILITY_NOTIF_APPEARS_AFTER_DAYS = 14
        private const val NOTIFICATION_ACTION_REQUEST_CODE = 3
        private const val NOTIFICATION_ALARM_REQUEST_CODE = 3
        private const val TAG = "UtilsNotificationHelper"

        /**
         * schedule an alarm to start custom action receiver to remind user to enable accessibility
         *
         * @param alarmReceiver - broadcastReceiver which listens to accessibility reminder action
         * @param alarmReceiverAction - custom accessibility reminder action
         */
        @JvmStatic
        fun setAccessibilityNotificationAlarm(context: Context, alarmReceiver: Class<out BroadcastReceiver>, alarmReceiverAction: String ,isOnlyForFirstInstall :Boolean) {
            setAccessibilityNotificationAlarm(context , alarmReceiver ,alarmReceiverAction ,DEFAULT_ACCESSIBILITY_NOTIF_APPEARS_AFTER_DAYS ,isOnlyForFirstInstall)
        }

        /**
         * schedule an alarm to start custom action receiver to remind user to enable accessibility
         *
         * @param alarmReceiver - broadcastReceiver which listens to accessibility reminder action
         * @param alarmReceiverAction - custom accessibility reminder action
         * @param defaultAppearsAfterDays - number of days after to show the notification
         * @param isOnlyForFirstInstall - if true will only set reminder for first tine install users
         */
        @JvmStatic
        fun setAccessibilityNotificationAlarm(context: Context, alarmReceiver: Class<out BroadcastReceiver>, alarmReceiverAction: String
                                              ,defaultAppearsAfterDays: Int ,isOnlyForFirstInstall :Boolean) {
            if (isSetAlarm(context ,isOnlyForFirstInstall) && !wasAccessibilityNotificationShown(context)) {
                Log.d(TAG, "accessibility notification reminder alarm is set")
                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val alarmIntent = Intent(context, alarmReceiver).let { intent ->
                    intent.action = alarmReceiverAction
                    PendingIntent.getBroadcast(context, NOTIFICATION_ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT)
                }
                val daysBefore = RemoteConfigHelper.getInteger(RC_KEY_ACCESSIBILITY_NOTIF_APPEARS_AFTER_DAYS
                , defaultAppearsAfterDays)
                val notifTime = NotificationUtils.getNextNotificationTime(daysBefore)
                Log.d(TAG, "notification should appear at " + SimpleDateFormat.getInstance().format(Date(notifTime)))
                alarmManager?.set(AlarmManager.RTC_WAKEUP, notifTime, alarmIntent)
                saveAccessibilityNotifAlarmSet(context)
            }
        }

        /**
         * show notification for accessibility reminder
         *
         * @param actionIntent - CTA intent to be started when user clicks on notification
         * @param notifText - notification message
         */
        @JvmStatic
        fun showAccessibilityNotification(context: Context, actionIntent: Intent, notifText: String) {
            try {
                RemoteConfigNotification(context)
                        .withRCKeyPrefix(RC_KEYS_ACCESSIBILITY_NOTIFICATION_PREFIX)
                        .withPendingIntent(PendingIntent.getActivity(context, NOTIFICATION_ACTION_REQUEST_CODE, actionIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                        .withNotificationText(notifText)
                        .withButtonText(context.getString(R.string.accessibility_notification_action_text))
                        .withExpandableContent(true)
                        .show()
                cancelAccessibilityNotificationAlarm(context)
            } catch (ex: Exception) {
                ExceptionLogger.logException(ex)
            }
        }

        /**
         * show notification for accessibility reminder
         *
         * @param context - activity to start from intent
         * @param notifText - notification message
         */
        @JvmStatic
        fun showAccessibilityNotificationRegular(context: Context, intent : Intent, notifText: String ,notifTitle: String
                , notificationIconResId : Int, channelId :String , notificationId:Int) {
            try {
                // Create the TaskStackBuilder
                val pendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
                    // Add the intent, which inflates the back stack
                    addNextIntentWithParentStack(intent)
                    // Get the PendingIntent containing the entire back stack
                    getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                }
                val builder = NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(notificationIconResId)
                        .setContentTitle(notifTitle)
                        .setContentText(notifText)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    // Create the NotificationChannel
                    val name =  context.getString(context.applicationInfo.labelRes); // app name by default
                    val importance = NotificationManager.IMPORTANCE_DEFAULT
                    val channel = NotificationChannel(channelId, name, importance)
                    // Register the channel with the system; you can't change the importance
                    // or other notification behaviors after this
                    val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.createNotificationChannel(channel)
                }

                with(NotificationManagerCompat.from(context)) {
                    // notificationId is a unique int for each notification that you must define

                    notify(notificationId, builder.build())


                }

                cancelAccessibilityNotificationAlarm(context)
            } catch (ex: Exception) {
                ExceptionLogger.logException(ex)
            }
        }

        /**
         * check if accessibility reminder alarm is already set
         */
        @JvmStatic
        fun isAccessibilityNotificationAlarmSet(context: Context) =
                PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY_PREF_ACCESSIBILITY_NOTIFICATION_ALARM_WAS_SET, false)

        /**
         * save flags not set up accessibility reminder alarm after notification was already received
         * we need both flags to reset alarm after device boot (alarms are cancelled when device turns off)
         * and not to set alarm each time an Application instance is created
         */
        private fun cancelAccessibilityNotificationAlarm(context: Context) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().remove(KEY_PREF_ACCESSIBILITY_NOTIFICATION_ALARM_WAS_SET).apply()
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_PREF_ACCESSIBILITY_NOTIFICATION_SHOWN, true).apply()
        }

        private fun wasAccessibilityNotificationShown(context: Context) =
                PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY_PREF_ACCESSIBILITY_NOTIFICATION_SHOWN, false)

        private fun saveAccessibilityNotifAlarmSet(context: Context) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_PREF_ACCESSIBILITY_NOTIFICATION_ALARM_WAS_SET, true).apply()
        }

        /**
         * check if app is just installed or updated
         * will return true in case isOnlyForFirstInstall is true and its first install
         * or if isOnlyForFirstInstall is false
         */
        private fun isSetAlarm(context: Context, isOnlyForFirstInstall :Boolean) : Boolean {
            try {
                if (!isOnlyForFirstInstall) return true
                val firstInstallTime = context.packageManager.getPackageInfo(context.packageName, 0).firstInstallTime
                val lastUpdateTime = context.packageManager.getPackageInfo(context.packageName, 0).lastUpdateTime
                return firstInstallTime == lastUpdateTime
            } catch (ex: PackageManager.NameNotFoundException) {
                ExceptionLogger.logException(ex)
                return  true
            }
        }


    }
}