package com.pitagoras.utilslib.interfaces

interface IExceptionLogger {
    fun logException(exception: Exception)
}
