package com.pitagoras.utilslib_demo;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.pitagoras.remoteconfigsdk.RemoteConfigHelper;
import com.pitagoras.utilslib.UtilsDialogManager;
import com.pitagoras.utilslib.UtilsNotificationHelper;
import com.pitagoras.utilslib_demo.callbacks.ClearBrowserPopupAnalyticCallback;

public class DemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
        RemoteConfigHelper.initializeRemoteConfig(this, R.xml.defaults );

        UtilsDialogManager.initBrowserHistoryHandler(this, new ClearBrowserPopupAnalyticCallback());
        if (!UtilsNotificationHelper.isAccessibilityNotificationAlarmSet(this)) {
            UtilsNotificationHelper.setAccessibilityNotificationAlarm(this,
                    NotificationAlarmReceiver.class, NotificationAlarmReceiver.ACCESSIBILITY_NOTIFICATION_ALARM ,false);
        }
    }
}
