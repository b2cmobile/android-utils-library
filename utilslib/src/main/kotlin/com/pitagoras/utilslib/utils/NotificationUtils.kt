package com.pitagoras.utilslib.utils

import android.util.Log
import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import java.util.*

class NotificationUtils {

    companion object {
        private const val TAG = "NotificationUtils"
        private const val ENABLE_ACCESSIBILITY_NOTIFICATION_TIME_START_0_24_HOURS = "enable_accessibility_notification_time_start_0_24_hours"
        private const val ENABLE_ACCESSIBILITY_NOTIFICATION_TIME_END_0_24_HOURS = "enable_accessibility__notification_time_end_0_24_hours"
        private const val REMOTE_CONFIG_DEFAULT_NOTIFICATION_START_TIME = 9
        private const val REMOTE_CONFIG_DEFAULT_NOTIFICATION_END_TIME = 21

        /**
         * get the next notification deleay ms. will return a time of day between the allowed times in order not to do notification at night
         * @param intervalInDays the number of days after which we want to show the notification
         */
        @JvmStatic
        fun getNextNotificationTime(intervalInDays: Int): Long {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_MONTH, intervalInDays)
            calendar.set(Calendar.HOUR_OF_DAY, 0) // reset hour
            calendar.set(Calendar.MINUTE, 0) // reset minutes
            calendar.set(Calendar.HOUR_OF_DAY, getHourOfADay())
            return calendar.timeInMillis
        }

        private fun getHourOfADay(): Int {
            val startTime = RemoteConfigHelper.getInteger(ENABLE_ACCESSIBILITY_NOTIFICATION_TIME_START_0_24_HOURS, REMOTE_CONFIG_DEFAULT_NOTIFICATION_START_TIME)
            val endTime = RemoteConfigHelper.getInteger(ENABLE_ACCESSIBILITY_NOTIFICATION_TIME_END_0_24_HOURS, REMOTE_CONFIG_DEFAULT_NOTIFICATION_END_TIME)
            val r = Random()
            val result = startTime + r.nextInt(endTime - startTime)
            Log.d(TAG, "Random hour between " + startTime + "and" + endTime + ": " + result)
            return result
        }
    }
}