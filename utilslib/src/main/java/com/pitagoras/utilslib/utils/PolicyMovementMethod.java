package com.pitagoras.utilslib.utils;

import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.widget.TextView;

import com.pitagoras.utilslib.interfaces.IDialogAnalytics;

public class PolicyMovementMethod extends LinkMovementMethod {

    private IDialogAnalytics mAnalyticsCallback;

    public PolicyMovementMethod(IDialogAnalytics aAnalyticsCallback) {
        mAnalyticsCallback = aAnalyticsCallback;
    }

    @Override
    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP && mAnalyticsCallback != null) {
            mAnalyticsCallback.onTextByLinkRead();
        }
        return super.onTouchEvent(widget, buffer, event);
    }
}