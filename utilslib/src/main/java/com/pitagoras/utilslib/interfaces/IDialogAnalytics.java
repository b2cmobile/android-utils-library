package com.pitagoras.utilslib.interfaces;

public interface IDialogAnalytics {

    void onDialogAppeared();

    void onTextByLinkRead();

    void onUserAccepted();
}