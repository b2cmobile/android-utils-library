package com.pitagoras.utilslib.interfaces

interface ICachedColorSelectionCallback {

    val isMainColorSpecified: Boolean

    val isColorSpecified: Boolean

    fun getLastSpecifiedMainColor(defColor: Int): Int

    fun getLastSpecifiedColor(defColor: Int): Int

    fun setLastSpecifiedMainColor(newColor: Int)

    fun setLastSpecifiedColor(newColor: Int)
}