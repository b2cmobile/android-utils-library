package com.pitagoras.utilslib.interfaces

interface IColorPickerWrapperCallback {

    fun onColorChanged(colorPickerId: Int, newColor: Int)
}