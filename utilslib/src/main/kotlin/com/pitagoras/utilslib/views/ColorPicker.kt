package com.pitagoras.utilslib.views

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView

import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.utils.*
import com.pitagoras.utilslib.ExceptionLogger

import java.util.ArrayList

class ColorPicker : GridLayout, View.OnClickListener {

    private lateinit var mColors: MutableSet<Int>
    private var mCurrentSelectedColorView: ImageView? = null

    private var mIsCheckMarkVisible = true
    private var mMinVisibleItemsInRow = DEFAULT_MIN_VISIBLE_ITEMS_IN_ROW
    private var mItemsMarginPx: Int = 0

    private var mColorChangedListener: ColorChangedListener? = null

    private var itemSelectionBorderWidthPx = 1 // 1 pixel width by default
    private var itemSelectionBorderColor = 0 // Transparent border by default

    var selectedColor: Int
        get() {
            return if (mCurrentSelectedColorView != null) {
                mCurrentSelectedColorView?.tag as Int
            } else {
                ExceptionLogger.logException(NullPointerException("Current Selected Color View is null!"))
                0
            }
        }
        set(color) {
            val imageView = findViewWithTag<ImageView>(color)
            mCurrentSelectedColorView?.setImageDrawable(null)
            mCurrentSelectedColorView = if (imageView != null) {
                setItemChecked(imageView, color)
                imageView
            } else {
                null
            }
        }

    val selectedView get() = mCurrentSelectedColorView

    val colors get() = mColors

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initAttributes(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttributes(context, attrs)
    }

    private fun initAttributes(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ColorPicker, 0, 0)
        try {
            mIsCheckMarkVisible = typedArray.getBoolean(R.styleable.ColorPicker_checkMarkVisible, true)
            mMinVisibleItemsInRow = typedArray.getFloat(R.styleable.ColorPicker_minItemsVisibleInRow, DEFAULT_MIN_VISIBLE_ITEMS_IN_ROW.toFloat())
            mItemsMarginPx = typedArray.getDimensionPixelSize(R.styleable.ColorPicker_itemsMargin, DEFAULT_ITEM_MARGIN)
        } finally {
            typedArray.recycle()
        }
    }

    fun initView(colors: MutableSet<Int>) {
        mColors = colors
        updateViews()
    }

    fun clear() {
        mColors.clear()
        removeAllViews()
    }

    fun setColorSelectionBorder(borderWidthPx: Int, borderColor: Int) {
        itemSelectionBorderWidthPx = borderWidthPx
        itemSelectionBorderColor = borderColor
    }

    private fun updateViews() {
        removeAllViews()
        for (color in mColors) {
            val imageView = ImageView(context)

            // setting layout params
            val layoutParams = GridLayout.LayoutParams()

            // getting size of the item according to the size of screen
            var visibleAreaWidth = resources.displayMetrics.widthPixels - paddingLeft// get width without padding
            if (mMinVisibleItemsInRow >= columnCount) {
                // if all columns fit the screen, right padding is also taken into consideration
                visibleAreaWidth -= paddingRight
            }
            val itemSizeInPx = (visibleAreaWidth / mMinVisibleItemsInRow).toInt() - 2 * mItemsMarginPx // 2 for left and right margins

            layoutParams.width = itemSizeInPx
            layoutParams.height = itemSizeInPx
            layoutParams.setMargins(mItemsMarginPx, mItemsMarginPx, mItemsMarginPx, mItemsMarginPx)
            imageView.layoutParams = layoutParams
            imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
            imageView.background = createBackground(color)
            imageView.setOnClickListener(this)
            imageView.tag = color
            addView(imageView)
        }
    }

    private fun setItemChecked(imageView: ImageView?, color: Int) {
        if (mIsCheckMarkVisible) {
            if (isColorDark(color)) {
                imageView?.setImageResource(R.drawable.ic_checkmark_white)
            } else {
                imageView?.setImageResource(R.drawable.ic_checkmark_dark)
            }
        }
    }

    private fun createBackground(color: Int): GradientDrawable {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.OVAL
        shape.setColor(color)
        if (itemSelectionBorderColor == 0) {
            if (!isColorDark(color)) {
                shape.setStroke(itemSelectionBorderWidthPx, resources.getColor(R.color.colorDefaultBorder))
            }
        } else {
            shape.setStroke(itemSelectionBorderWidthPx, itemSelectionBorderColor)
        }
        return shape
    }

    override fun onClick(v: View) {
        if (v === mCurrentSelectedColorView) {
            return  // if it's the same view, then do nothing
        }
        val selectedColor = v.tag as Int
        mCurrentSelectedColorView?.setImageDrawable(null)
        mCurrentSelectedColorView = v as ImageView
        setItemChecked(mCurrentSelectedColorView, selectedColor)
        mColorChangedListener?.onColorChanged(id, ArrayList(mColors).indexOf(selectedColor), selectedColor)
    }

    fun setColorChangedListener(aColorChangedListener: ColorChangedListener) {
        mColorChangedListener = aColorChangedListener
    }

    interface ColorChangedListener {
        fun onColorChanged(colorPickerId: Int, position: Int, newColor: Int)
    }

    companion object {
        const val DEFAULT_MIN_VISIBLE_ITEMS_IN_ROW = 5f
        const val DEFAULT_ITEM_MARGIN = 10
    }
}
