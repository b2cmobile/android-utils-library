package com.pitagoras.utilslib.models

enum class ClearBrowserRemoteConfigKeys(val paramName: String) {
    BROWSER_CONFIG("browsers_config"),
    POPUP_SHOULD_BE_SHOWN("clear_browser_popup_should_be_shown"),
    POPUP_SHOULD_BE_SHOWN_V2("clear_browser_popup_should_be_shown_v2"),
    POPUP_SHOW_INTERVAL_DAYS("clear_browser_popup_show_interval_days"),
    POPUP_BASE_SERVER_URL("clear_browser_popup_base_server_url"),
    POPUP_BACKGROUND_COLOR("clear_browser_popup_background_color"),
    POPUP_TITLE_TEXT_COLOR("clear_browser_popup_title_text_color"),
    POPUP_SUBTITLE_TEXT_COLOR("clear_browser_popup_subtitle_text_color"),
    POPUP_DESCRIPTION_TEXT_COLOR("clear_browser_popup_description_text_color"),
    POPUP_GOT_IT_TEXT("clear_browser_popup_rate_text"),
    POPUP_BUTTON_TEXT_COLOR("clear_browser_popup_button_text_color"),
    POPUP_BUTTON_COLOR("clear_browser_popup_button_color")
}