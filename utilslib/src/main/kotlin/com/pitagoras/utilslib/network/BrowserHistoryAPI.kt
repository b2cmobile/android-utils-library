package com.pitagoras.utilslib.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface BrowserHistoryAPI {

    @GET("/needToClearBrowsing")
    fun needToClearBrowsing(@Query("uuid") uuid: String, @Query("aaa") countOfVisitedUrls: Int): Call<BrowserHistoryResponse>

    companion object {
        fun create(baseServerUrl: String): BrowserHistoryAPI {
            return Retrofit.Builder()
                    .baseUrl(baseServerUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create<BrowserHistoryAPI>(BrowserHistoryAPI::class.java)
        }
    }
}
