package com.pitagoras.utilslib.interfaces

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

internal class DefaultColorSelectionCallback(context: Context) : ICachedColorSelectionCallback {
    private val mSharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)

    override val isColorSpecified: Boolean
        get() = mSharedPreferences.contains(SELECTED_COLOR_PREF_KEY)

    override val isMainColorSpecified: Boolean
        get() = mSharedPreferences.contains(SELECTED_COLOR_MAIN_PREF_KEY)

    override fun getLastSpecifiedColor(defColor: Int): Int {
        return mSharedPreferences.getInt(SELECTED_COLOR_PREF_KEY, defColor)
    }

    override fun setLastSpecifiedColor(newColor: Int) {
        mSharedPreferences.edit().putInt(SELECTED_COLOR_PREF_KEY, newColor).apply()
    }

    override fun getLastSpecifiedMainColor(defColor: Int): Int {
        return mSharedPreferences.getInt(SELECTED_COLOR_MAIN_PREF_KEY, defColor)
    }

    override fun setLastSpecifiedMainColor(newColor: Int) {
        mSharedPreferences.edit().putInt(SELECTED_COLOR_MAIN_PREF_KEY, newColor).apply()
    }

    companion object {
        private const val SELECTED_COLOR_MAIN_PREF_KEY = "def_selected_color_main_pref_key"
        private const val SELECTED_COLOR_PREF_KEY = "def_selected_color_pref_key"
    }
}
