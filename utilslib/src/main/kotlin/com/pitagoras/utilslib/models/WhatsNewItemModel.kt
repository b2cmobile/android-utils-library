package com.pitagoras.utilslib.models

class WhatsNewItemModel(val sectionIconId: Int, val sectionTitle: String, val sectionDescription: String) {
    override fun toString(): String {
        return "sectionTitle: $sectionTitle; sectionDescription: $sectionDescription"
    }
}