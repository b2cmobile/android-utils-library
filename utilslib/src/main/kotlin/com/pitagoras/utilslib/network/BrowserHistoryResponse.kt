package com.pitagoras.utilslib.network

import com.google.gson.annotations.SerializedName

data class BrowserHistoryResponse(@SerializedName("show") val needToClearBrowsing: Boolean)
