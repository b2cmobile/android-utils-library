package com.pitagoras.utilslib_demo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.pitagoras.remoteconfigsdk.RemoteConfigHelper;
import com.pitagoras.url_retriever.UrlRetrieverHandler;
import com.pitagoras.url_retriever.utils.UrlRetrieverHelper;
import com.pitagoras.utilslib.UtilsDialogManager;
import com.pitagoras.utilslib.interfaces.ICachedColorSelectionCallback;
import com.pitagoras.utilslib.interfaces.IColorPickerWrapperCallback;

public class MainActivity extends AppCompatActivity implements IColorPickerWrapperCallback, ICachedColorSelectionCallback {

    private static final String SELECTED_COLOR_MAIN_PREF_KEY = "selected_color_main_pref_key";
    private static final String SELECTED_COLOR_PREF_KEY = "selected_color_pref_key";
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

//        ColorPickerWrapper colorPickerWrapper = new ColorPickerWrapper(findViewById(R.id.colorPickerRootView), this);
//        colorPickerWrapper.setCachedColorSelectionCallback(this);
//        colorPickerWrapper.setUseDefaultColorAtFirstLaunch(true);
//        colorPickerWrapper.init();
        findViewById(R.id.dialogTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilsDialogManager.showWhatsNewPopupIfNeeded(MainActivity.this, null);
            }
        });

        findViewById(R.id.dialogClearBrowserButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilsDialogManager.showClearBrowsingPopupIfNeeded(MainActivity.this);
            }
        });

        findViewById(R.id.enableAccessibilityBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UrlRetrieverHelper.isAccessibilitySettingsEnabled(getApplicationContext(), AccessibilityWrapper.class)) {
                    Toast.makeText(MainActivity.this, "Accessibility already enabled!", Toast.LENGTH_SHORT).show();
                } else {
                    UrlRetrieverHandler.activityToStartWhenConnected = MainActivity.class;
                    UrlRetrieverHelper.showAccessibilitySettings(MainActivity.this);
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        UtilsDialogManager.cancelBrowserHistoryPopup();
    }

    @Override
    protected void onResume() {
        super.onResume();
        RemoteConfigHelper.update(this);
        // UtilsDialogManager.showClearBrowsingPopupIfNeeded(MainActivity.this);
    }

    @Override
    public boolean isMainColorSpecified() {
        return mSharedPreferences.contains(SELECTED_COLOR_MAIN_PREF_KEY);
    }

    @Override
    public boolean isColorSpecified() {
        return mSharedPreferences.contains(SELECTED_COLOR_PREF_KEY);
    }

    @Override
    public int getLastSpecifiedMainColor(int defColor) {
        return mSharedPreferences.getInt(SELECTED_COLOR_MAIN_PREF_KEY, defColor);
    }

    @Override
    public int getLastSpecifiedColor(int defColor) {
        return mSharedPreferences.getInt(SELECTED_COLOR_PREF_KEY, defColor);
    }

    @Override
    public void setLastSpecifiedMainColor(int newColor) {
        mSharedPreferences.edit().putInt(SELECTED_COLOR_MAIN_PREF_KEY, newColor).apply();
    }

    @Override
    public void onColorChanged(int colorPickerId, int newColor) {
        findViewById(colorPickerId).setBackgroundColor(newColor);
    }

    @Override
    public void setLastSpecifiedColor(int newColor) {
        mSharedPreferences.edit().putInt(SELECTED_COLOR_PREF_KEY, newColor).apply();
    }
}