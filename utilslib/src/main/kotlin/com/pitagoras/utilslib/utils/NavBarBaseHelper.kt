package com.pitagoras.utilslib.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.PixelFormat
import android.app.Activity
import android.util.DisplayMetrics
import android.graphics.Point
import android.os.Build
import android.view.Gravity
import android.view.WindowManager
import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import com.pitagoras.utilslib.ExceptionLogger

private const val PORTRAIT_0 = 0
private const val LANDSCAPE_90 = 1
private const val PORTRAIT_180 = 2
private const val LANDSCAPE_270 = 3

private const val NAV_BAR_HEIGHT = "navigation_bar_height"
private const val DIMEN = "dimen"
private const val ANDROID = "android"
private const val NAV_BAR_HEIGHT_LANDSCAPE = "navigation_bar_height_landscape"

// smallest tablet width or height in dp
// is using to check if current smallestScreenWidthDp is lower than specified value, if no, we can assume that it's a tablet
// on tablets, navigation bar is located always in the bottom of the screen, no matter if it's portrait or landscape
private const val MIN_TABLET_SCREEN_IN_DP_REMOTE_KEY = "min_tablet_screen_in_dp"
private const val DEF_MIN_TABLET_SCREEN_IN_DP = 600

/**
 * Landscape orientation doesn't mean that navigation bar is on the right/left side.
 * For tablets it will be always on the bottom
 *
 * @return true if navigation bar is located on bottom of the screen
 */
fun isNavigationBarOnBottom(context: Context, rotation: Int): Boolean {
    return when (rotation) {
        PORTRAIT_0, PORTRAIT_180 -> true
        LANDSCAPE_90, LANDSCAPE_270 -> {
            val resources = context.resources
            val configuration = resources.configuration
            val displayMetrics = resources.displayMetrics
            val canMove = displayMetrics.widthPixels != displayMetrics.heightPixels &&
                    configuration.smallestScreenWidthDp < RemoteConfigHelper.getInteger(MIN_TABLET_SCREEN_IN_DP_REMOTE_KEY, DEF_MIN_TABLET_SCREEN_IN_DP)
            !canMove || displayMetrics.widthPixels < displayMetrics.heightPixels
        }
        else -> {
            ExceptionLogger.logException(IllegalStateException("Unhandled orientation: $rotation"))
            true
        }
    }
}

/**
 * On devices below 24, in landscape 270, navigation bar will be located on the right side, on above - left side
 *
 * @param rotation value from WindowManager.getDefaultDisplay().getRotation()
 * @return true if in landscape 270 navigation bar is on left side
 */
fun isLandscapeNavigationBarGravityLeft(rotation: Int) = Build.VERSION.SDK_INT >= 24 && rotation == LANDSCAPE_270

val overlayFlags: Int
    get() = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE or
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS

val layoutParamsType: Int
    get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
    } else {
        WindowManager.LayoutParams.TYPE_PHONE
    }

fun getNavigationBarHeight(resources: Resources): Int {
    var resourceId = resources.getIdentifier(NAV_BAR_HEIGHT, DIMEN, ANDROID)
    if (resourceId > 0) {
        val height = resources.getDimensionPixelSize(resourceId)
        // on LG G6(7.0) it will be always 0
        if (height == 0) {
            resourceId = resources.getIdentifier(NAV_BAR_HEIGHT_LANDSCAPE, DIMEN, ANDROID)
            if (resourceId > 0) {
                return resources.getDimensionPixelSize(resourceId)
            } else {
                ExceptionLogger.logException(Resources.NotFoundException("Height of navigation bar wasn't found"))
            }
        } else {
            return height
        }
    }
    return 0
}

fun getNavBarLayoutParams(context: Context, windowManager: WindowManager?): WindowManager.LayoutParams {
    val displaySize = Point()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        windowManager?.defaultDisplay?.getRealSize(displaySize)
    } else {
        displaySize.x = windowManager?.defaultDisplay?.width ?: 0
        displaySize.y = windowManager?.defaultDisplay?.height ?: 0
    }
    val rotation = windowManager?.defaultDisplay?.rotation ?: 0
    return if (isNavigationBarOnBottom(context, rotation)) {
        getPortraitLayoutParams(displaySize, context.resources)
    } else {
        getLandscapeLayoutParams(displaySize, context.resources, rotation)
    }
}

private fun getPortraitLayoutParams(displaySize: Point, resources: Resources): WindowManager.LayoutParams {
    val layoutParams = WindowManager.LayoutParams(displaySize.x,
            getNavigationBarHeight(resources), layoutParamsType,
            overlayFlags, PixelFormat.TRANSLUCENT)
    layoutParams.gravity = Gravity.LEFT or Gravity.BOTTOM
    layoutParams.y = -getNavigationBarHeight(resources)
    layoutParams.x = 0
    return layoutParams
}

private fun getLandscapeLayoutParams(displaySize: Point, resources: Resources, rotation: Int): WindowManager.LayoutParams {
    val layoutParams = WindowManager.LayoutParams(
            getNavigationBarHeight(resources), displaySize.y,
            layoutParamsType, overlayFlags, PixelFormat.TRANSLUCENT)
    layoutParams.gravity = Gravity.BOTTOM or
            if (isLandscapeNavigationBarGravityLeft(rotation)) {
                Gravity.LEFT
            } else {
                Gravity.RIGHT
            }
    layoutParams.x = -getNavigationBarHeight(resources)
    return layoutParams
}

/**
 * Check if there is software navigation bar on device by comparing both
 * real size of display and and available for app window
 *
 * @return true if device has a software navigation bar
 */
fun isSoftNavBar(windowManager: WindowManager): Boolean {

    val display = windowManager.getDefaultDisplay()

    // Get display metrics based on the real size of display.
    val realDisplayMetrics = DisplayMetrics()
    display.getRealMetrics(realDisplayMetrics)
    val realHeight = realDisplayMetrics.heightPixels
    val realWidth = realDisplayMetrics.widthPixels

    // Get available metrics for current app window
    val displayMetrics = DisplayMetrics()
    display.getMetrics(displayMetrics)
    val displayHeight = displayMetrics.heightPixels
    val displayWidth = displayMetrics.widthPixels

    return realWidth > displayWidth || realHeight > displayHeight
}
