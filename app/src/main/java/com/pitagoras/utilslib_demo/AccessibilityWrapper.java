package com.pitagoras.utilslib_demo;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.pitagoras.url_retriever.UrlRetrieverHandler;
import com.pitagoras.url_retriever.callbacks.IAccessibilityEventHandler;
import com.pitagoras.url_retriever.callbacks.IRootInActiveWindowCallback;
import com.pitagoras.utilslib.UtilsDialogManager;

public class AccessibilityWrapper extends AccessibilityService implements IRootInActiveWindowCallback {

    private static final String TAG = "AccessibilityWrapper";
    private static final String EXTRA_BACK_PRESS = "back_press";

    private IAccessibilityEventHandler mAccessibilityEvent;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent aAccessibilityEvent) {
        if (checkAndConfigureUrlHandler()) {
            mAccessibilityEvent.onAccessibilityEvent(aAccessibilityEvent);
        }
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d(TAG, "Service connected");
        checkAndConfigureUrlHandler();
    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "Accessibility Interrupted!");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Accessibility Destroyed!");
        if (mAccessibilityEvent != null) {
            mAccessibilityEvent.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getBooleanExtra(EXTRA_BACK_PRESS, false)) {
            performGlobalAction(GLOBAL_ACTION_BACK);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Check if browser history handler is enabled on RC.
     * If it's disabled, it will destroy instance of UrlRetrieverHandler.
     * If enabled, new instance will be created if it.
     *
     * @return true if browser history handler enabled on RC.
     */
    private boolean checkAndConfigureUrlHandler() {
        if (UtilsDialogManager.isClearBrowserPopupEnabled()) {
            if (mAccessibilityEvent == null) {
                mAccessibilityEvent = new UrlRetrieverHandler(this);
                mAccessibilityEvent.onConnected(UtilsDialogManager.getBrowsersConfigurations(this));
            }
            return true;
        } else {
            if (mAccessibilityEvent != null) {
                mAccessibilityEvent.onDestroy();
                mAccessibilityEvent = null;
            }
            return false;
        }
    }
}
