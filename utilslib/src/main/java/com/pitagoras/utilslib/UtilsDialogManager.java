package com.pitagoras.utilslib;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.pitagoras.remoteconfigsdk.RemoteConfigHelper;
import com.pitagoras.url_retriever.callbacks.IAccessibilitySettingsStatusCallback;
import com.pitagoras.url_retriever.callbacks.IExceptionLogging;
import com.pitagoras.url_retriever.utils.UrlRetrieverHelper;
import com.pitagoras.utilslib.activities.AccessibilityHintActivity;
import com.pitagoras.utilslib.dialogs.ClearBrowsingHistoryDialogFragment;
import com.pitagoras.utilslib.dialogs.DefaultDialogFragment;
import com.pitagoras.utilslib.dialogs.WhatsNewDialogFragment;
import com.pitagoras.utilslib.interfaces.IAccessibilityHintActions;
import com.pitagoras.utilslib.interfaces.IAccessibilityHintAppearanceParams;
import com.pitagoras.utilslib.interfaces.IClearBrowserPopupAnalyticsCallback;
import com.pitagoras.utilslib.interfaces.IDialogAnalytics;
import com.pitagoras.utilslib.interfaces.IDialogAppearance;
import com.pitagoras.utilslib.interfaces.IDialogRemoteConfigParams;
import com.pitagoras.utilslib.interfaces.WhatsNewActionsCallback;
import com.pitagoras.utilslib.models.ClearBrowserRemoteConfigKeys;
import com.pitagoras.utilslib.models.WhatsNewRemoteConfigKeys;
import com.pitagoras.utilslib.network.BrowserHistoryHandler;

public class UtilsDialogManager {

    private static final String TAG = UtilsDialogManager.class.getSimpleName();

    private static IAccessibilityHintActions sAccessibilityHintActions;
    private static IClearBrowserPopupAnalyticsCallback sClearBrowserPopupAnalyticsCallback;
    private static BrowserHistoryHandler sBrowserHistoryHandler;

    public static void initializeDialogManager(Context context, int aDefaultsResId) {
        RemoteConfigHelper.initializeRemoteConfig(context, aDefaultsResId);
    }

    /**
     * Helper method to show popup after app update to notify user about some information.
     */
    public static void showDialogIfNeeded(AppCompatActivity aActivity,
                                          IDialogAppearance aDialogAppearance,
                                          IDialogAnalytics aAnalyticsCallback,
                                          IDialogRemoteConfigParams aDialogRemoteConfigParams) {
        if (RemoteConfigHelper.getBoolean(aDialogRemoteConfigParams.isShowDialogKey(), true)) {
            if (!PreferenceManager.getDefaultSharedPreferences(aActivity)
                    .getBoolean(DefaultDialogFragment.KEY_PREF_USER_ACCEPTED, false)) {
                FragmentTransaction ft = aActivity.getSupportFragmentManager().beginTransaction();
                DefaultDialogFragment fragment = (DefaultDialogFragment) aActivity
                        .getSupportFragmentManager()
                        .findFragmentByTag(DefaultDialogFragment.class.getSimpleName());
                if (fragment != null) {
                    ft.remove(fragment);
                }

                String message = RemoteConfigHelper
                        .getString(aDialogRemoteConfigParams.getDialogMessageKey(),
                                aActivity.getString(R.string.pp_message));

                String acceptText = RemoteConfigHelper
                        .getString(aDialogRemoteConfigParams.getDialogAcceptTextKey(),
                                aActivity.getString(R.string.pp_accept_text));

                fragment = new DefaultDialogFragment.Builder()
                        .setThemeResId(aDialogAppearance.getDialogThemeResId())
                        .setIconResId(aDialogAppearance.getDialogIconResId())
                        .setDialogMessage(message)
                        .setAcceptButtonText(acceptText)
                        .create();

                fragment.setAnalyticsCallback(aAnalyticsCallback);
                fragment.show(ft, DefaultDialogFragment.class.getSimpleName());
            }
        } else {
            resetDialogAppearance(aActivity);
        }
    }

    /**
     * reset USER_PRESSED_GOT_IT flag for updated popup
     * when USER_PRESSED_GOT_IT flag is true - this means there is no need to show popup any more
     */
    public static void resetDialogAppearance(Context aContext) {
        PreferenceManager
                .getDefaultSharedPreferences(aContext)
                .edit()
                .putBoolean(DefaultDialogFragment.KEY_PREF_USER_ACCEPTED, false)
                .apply();
    }

    /**
     * helper method to notify user about app novelties
     *
     * @param activity    - activity context. Can implement {@link com.pitagoras.utilslib.interfaces.WhatsNewActionsCallback}
     * @param fragmentTag - tag of a fragment from which the method is called.
     *                    If set, fragment must implement {@link com.pitagoras.utilslib.interfaces.WhatsNewActionsCallback}
     * @return true if dialog was shown
     */
    public static boolean showWhatsNewPopupIfNeeded(AppCompatActivity activity, String fragmentTag) {
        if (RemoteConfigHelper.getBoolean(WhatsNewRemoteConfigKeys.POPUP_SHOULD_BE_SHOWN.getParamName(), false)) {
            if (!PreferenceManager.getDefaultSharedPreferences(activity)
                    .getBoolean(WhatsNewDialogFragment.KEY_PREF_POPUP_WAS_SHOWN, false)) {
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                WhatsNewDialogFragment fragment = (WhatsNewDialogFragment) activity
                        .getSupportFragmentManager()
                        .findFragmentByTag(WhatsNewDialogFragment.class.getSimpleName());
                if (fragment != null) {
                    ft.remove(fragment);
                }
                fragment = WhatsNewDialogFragment.Companion.newInstance(fragmentTag);
                try {
                    fragment.show(ft, WhatsNewDialogFragment.class.getSimpleName());
                }
                catch (IllegalStateException e) {
                    ExceptionLogger.INSTANCE.logException(e);
                    Log.d(TAG,"not showing whats new");
                    return false;
                }


                ((WhatsNewActionsCallback) (TextUtils.isEmpty(fragmentTag) ? activity : activity.getSupportFragmentManager().findFragmentByTag(fragmentTag))).onPopupShown();
                saveWhatsNewWasShownFlag(activity, true);
                return true;
            }
        } else {
            saveWhatsNewWasShownFlag(activity, false);
        }
        return false;
    }

    /**
     * Initialize all needed callbacks for getting and storing count of visited URLs
     *
     * @param context                            any context, which will be used to store data in preferences
     * @param clearBrowserPopupAnalyticsCallback analytics callback
     */
    public static void initBrowserHistoryHandler(Context context, IClearBrowserPopupAnalyticsCallback clearBrowserPopupAnalyticsCallback) {
        sBrowserHistoryHandler = new BrowserHistoryHandler(context);
        sClearBrowserPopupAnalyticsCallback = clearBrowserPopupAnalyticsCallback;

        UrlRetrieverHelper.setExceptionLogging(new IExceptionLogging() {
            @Override
            public void logCrash(Exception e) {
                ExceptionLogger.INSTANCE.logException(e);
            }
        });
        UrlRetrieverHelper.setUrlAvailableCallback(sBrowserHistoryHandler);
        UrlRetrieverHelper.setApplicationCallback(sBrowserHistoryHandler);
        UrlRetrieverHelper.setAccessibilitySettingsStatusCallback(new IAccessibilitySettingsStatusCallback() {
            @Override
            public void onAccessibilityStatusChanged(String category, String action, String label) {
                if (sClearBrowserPopupAnalyticsCallback != null) {
                    sClearBrowserPopupAnalyticsCallback.onAccessibilityStatusChanged(category, action, label);
                }
            }
        });
    }

    /**
     * Helper method to check if clear browse history popup should be shown to user.
     *
     * @param activity - activity context.
     */
    public static void showClearBrowsingPopupIfNeeded(AppCompatActivity activity) {
        if (!isClearBrowserPopupEnabled()) {
            Log.d(TAG, "Browsing popup is disabled by RC.");
            return;
        }

        if (sBrowserHistoryHandler == null) {
            ExceptionLogger.INSTANCE.logException(new IllegalStateException("BrowserHistoryHandler wasn't initialize"));
            return;
        }
        sBrowserHistoryHandler.showClearBrowsingPopupIfNeeded(activity);
    }

    /**
     * Cancel request to server if it was sent.
     */
    public static void cancelBrowserHistoryPopup() {
        if (sBrowserHistoryHandler != null) {
            sBrowserHistoryHandler.cancelBrowserHistoryPopup();
        }
    }

    /**
     * Create and show clear browsing history dialog.
     *
     * @param activity - activity context.
     * @return true if dialog was shown
     */
    public static boolean showClearBrowsingPopup(AppCompatActivity activity) {
        if (!isClearBrowserPopupEnabled()) {
            Log.d(TAG, "Browsing popup is disabled by RC.");
            return false;
        }

        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        ClearBrowsingHistoryDialogFragment fragment = (ClearBrowsingHistoryDialogFragment) activity
                .getSupportFragmentManager()
                .findFragmentByTag(ClearBrowsingHistoryDialogFragment.class.getSimpleName());
        if (fragment != null) {
            fragmentTransaction.remove(fragment);
        }
        fragment = new ClearBrowsingHistoryDialogFragment();
        try {
            fragment.show(fragmentTransaction, ClearBrowsingHistoryDialogFragment.class.getSimpleName());
        } catch (IllegalStateException e) {
            ExceptionLogger.INSTANCE.logException(e);
            Log.d(TAG, "not showing clear browsing", e);
            return false;
        }
        if (sClearBrowserPopupAnalyticsCallback != null) {
            sClearBrowserPopupAnalyticsCallback.onClearBrowserPopupShown();
        }
        return true;
    }

    /**
     * @return clear browsing analytics callback
     */
    public static IClearBrowserPopupAnalyticsCallback getClearBrowserPopupAnalyticsCallback() {
        return sClearBrowserPopupAnalyticsCallback;
    }

    /**
     * @param context any context to get string from resources
     * @return configuration for browsers in JSON format
     */
    public static String getBrowsersConfigurations(Context context) {
        return RemoteConfigHelper.getString(ClearBrowserRemoteConfigKeys.BROWSER_CONFIG.getParamName(),
                context.getString(R.string.browsers_config));
    }

    /**
     * Check if clear browser popup is enabled on RC, value is true by default.
     *
     * @return true if clear browser popup is enabled on RC.
     */
    public static boolean isClearBrowserPopupEnabled() {
        return RemoteConfigHelper.getBoolean(ClearBrowserRemoteConfigKeys.POPUP_SHOULD_BE_SHOWN_V2.getParamName(), true);
    }

    /**
     * @param markAsShown - set flag to true to show dialog only once after app update
     *                    clear flag (set to false) when RC WhatsNewRemoteConfigKeys.POPUP_SHOULD_BE_SHOWN is set to false
     */
    private static void saveWhatsNewWasShownFlag(AppCompatActivity activity, boolean markAsShown) {
        PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean(WhatsNewDialogFragment.KEY_PREF_POPUP_WAS_SHOWN, markAsShown).apply();
    }

    /**
     * helper method to show accessibility hint when user is redirected to settings
     *
     * @param appearanceParams - implementation of {@link IAccessibilityHintAppearanceParams} to setup hint view
     * @param actionsCallback  - implementation of {@link IAccessibilityHintActions} to handle user interaction with hint
     */
    public static void requestAccessibilityWithHint(Context context,
                                                    IAccessibilityHintAppearanceParams appearanceParams,
                                                    IAccessibilityHintActions actionsCallback) {
        if (context == null || appearanceParams == null || actionsCallback == null) {
            Log.d(TAG, "showAccessibilityHint params not set correctly");
        } else {
            sAccessibilityHintActions = actionsCallback;
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(intent);
            AccessibilityHintActivity.Companion.showAccessibilityHint(context, appearanceParams);
        }
    }

    /**
     * handle click on GOT IT! button on accessibility hint
     */
    public static void onAccessibilityHintButtonClick() {
        if (sAccessibilityHintActions != null) {
            sAccessibilityHintActions.onGotItButtonClick();
        } else {
            Log.d(TAG, "onAccessibilityHintButtonClick IAccessibilityHintActions is null");
        }
    }
}