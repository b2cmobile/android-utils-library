package com.pitagoras.utilslib.interfaces

interface IClearBrowserPopupAnalyticsCallback {

    fun onGotItClicked()

    fun onClearBrowserPopupShown()

    fun onClearBrowserPopupClosed()

    fun onAccessibilityStatusChanged(category: String, action: String, label: String)

    fun onBrowserHistoryServerRequestSent(countOfUrls: Int)
}