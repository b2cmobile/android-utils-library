package com.pitagoras.utilslib.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pitagoras.remoteconfigsdk.ViewConfigHelper
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.UtilsDialogManager
import com.pitagoras.utilslib.interfaces.IAccessibilityHintAppearanceParams
import com.pitagoras.utilslib.models.AccessibilityHintRemoteConfigParams

class AccessibilityHintActivity : AppCompatActivity() {
    companion object {
        private const val BUNDLE_KEY_APP_ICON_RES_ID = "bundle_key_app_icon_res_id"
        private const val BUNDLE_KEY_HINT_GIF_RES_ID = "bundle_key_hint_gif_res_id"
        private const val BUNDLE_KEY_HINT_TITLE_TEXT_RES_ID = "bundle_key_hint_title_text_res_id"

        private const val ANIMATION_ACTIVITY_OPEN_TIME_IN_MILLIS: Long = 500

        fun showAccessibilityHint(context: Context, appearanceParams: IAccessibilityHintAppearanceParams) {
            Handler().postDelayed({
                val intent = Intent(context, AccessibilityHintActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                intent.putExtra(BUNDLE_KEY_APP_ICON_RES_ID, appearanceParams.getAppIconResId())
                intent.putExtra(BUNDLE_KEY_HINT_GIF_RES_ID, appearanceParams.getHintGifResId())
                intent.putExtra(BUNDLE_KEY_HINT_TITLE_TEXT_RES_ID, appearanceParams.getHintTitleTextResId())
                context.startActivity(intent)
            }, ANIMATION_ACTIVITY_OPEN_TIME_IN_MILLIS)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accessibility_hint)
        initViews()
    }

    public override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
        finish()
    }

    private fun initViews() {
        val appIconResId = intent.getIntExtra(BUNDLE_KEY_APP_ICON_RES_ID, 0)
        val gifResId = intent.getIntExtra(BUNDLE_KEY_HINT_GIF_RES_ID, 0)
        val titleText = intent.getIntExtra(BUNDLE_KEY_HINT_TITLE_TEXT_RES_ID, 0)
        findViewById<ImageView>(R.id.accessibilityHintAppIconImageView).setImageResource(appIconResId)
        if (gifResId != 0) {
            Glide.with(this).asGif().load(gifResId).into(findViewById(R.id.accessibilityHintImageView))
        }
        findViewById<TextView>(R.id.accessibilityHintTitleTextView).text = if (titleText == 0) null else getText(titleText)
        setRemoteConfigValues()
        findViewById<Button>(R.id.accessibilityHintGotItButton).setOnClickListener {
            UtilsDialogManager.onAccessibilityHintButtonClick()
            finish()
        }
    }

    private fun setRemoteConfigValues() {
        val titleTextView = findViewById<TextView>(R.id.accessibilityHintTitleTextView)
        ViewConfigHelper.setTextViewText(titleTextView,
                AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_TITLE_TEXT.paramName,
                "")
        ViewConfigHelper.setTextViewTextColor(titleTextView,
                AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_TITLE_TEXT_COLOR.paramName)

        ViewConfigHelper.setViewBackgroundColor(findViewById(R.id.accessibilityHintTitleLayout),
                AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_TITLE_BACKGROUND_COLOR.paramName)

        val gotItButton = findViewById<Button>(R.id.accessibilityHintGotItButton)
        ViewConfigHelper.setButtonText(gotItButton, AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_BUTTON_TEXT.paramName, "")
        ViewConfigHelper.setButtonTextColor(gotItButton, AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_BUTTON_TEXT_COLOR.paramName)
        ViewConfigHelper.setButtonBackgroundColor(gotItButton, AccessibilityHintRemoteConfigParams.ACCESSIBILITY_HINT_BUTTON_BACKGROUND_COLOR.paramName)
    }
}