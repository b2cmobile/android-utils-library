package com.pitagoras.utilslib.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.pitagoras.remoteconfigsdk.ViewConfigHelper
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.models.WhatsNewItemModel
import com.pitagoras.utilslib.models.WhatsNewRemoteConfigKeys

class WhatsNewAdapter(private val context: Context, private val sections: ArrayList<WhatsNewItemModel>) : BaseExpandableListAdapter() {

    companion object {
        private const val TAG = "WhatsNewAdapter"
        private const val FIELD_CHILDREN_COUNT = 1
    }

    override fun getGroup(groupPosition: Int): WhatsNewItemModel = sections.get(index = groupPosition)

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = false

    override fun hasStableIds() = true

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val groupView: View
        val viewHolder: GroupViewHolder
        if (convertView == null) {
            Log.d(TAG, "getGroupView: create GroupViewHolder for position = $groupPosition")
            groupView = LayoutInflater.from(context).inflate(R.layout.whats_new_item_group_layout, parent, false)
            viewHolder = GroupViewHolder(groupView)
            groupView.tag = viewHolder
        } else {
            groupView = convertView
            viewHolder = groupView.tag as GroupViewHolder
            Log.d(TAG, "getGroupView: GroupViewHolder exists for position = $groupPosition")
        }
        val section = getGroup(groupPosition)
        Log.d(TAG, "section model for groupPosition = $groupPosition: $section")
        viewHolder.sectionIconImageView.setImageResource(section.sectionIconId)
        viewHolder.sectionTitleTextView.text = section.sectionTitle
        ViewConfigHelper.setTextViewTextColor(viewHolder.sectionTitleTextView, WhatsNewRemoteConfigKeys.POPUP_SUBTITLE_TEXT_COLOR.paramName)
        return groupView
    }

    override fun getChildrenCount(groupPosition: Int) = FIELD_CHILDREN_COUNT

    override fun getChild(groupPosition: Int, childPosition: Int) = getGroup(groupPosition).sectionDescription

    override fun getGroupId(groupPosition: Int) = groupPosition.toLong()

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        val childView = (convertView ?: LayoutInflater.from(context).inflate(R.layout.whats_new_item_child_layout, parent, false)) as TextView
        childView.text = getChild(groupPosition, childPosition)
        ViewConfigHelper.setTextViewTextColor(childView, WhatsNewRemoteConfigKeys.POPUP_DESCRIPTION_TEXT_COLOR.paramName)
        return childView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int) = childPosition.toLong()

    override fun getGroupCount() = sections.size

    class GroupViewHolder(groupView: View) {
        val sectionIconImageView: ImageView = groupView.findViewById(R.id.imageViewWhatsNewGroupItemIcon)
        val sectionTitleTextView: TextView = groupView.findViewById(R.id.textViewWhatsNewGroupItemSection)
    }
}