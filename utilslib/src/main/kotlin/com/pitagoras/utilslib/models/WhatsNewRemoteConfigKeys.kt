package com.pitagoras.utilslib.models

enum class WhatsNewRemoteConfigKeys(val paramName: String) {
    POPUP_SHOULD_BE_SHOWN("whats_new_popup_should_be_shown"),
    POPUP_BACKGROUND_COLOR("whats_new_popup_background_color"),
    POPUP_TITLE_TEXT_COLOR("whats_new_popup_title_text_color"),
    POPUP_SUBTITLE_TEXT_COLOR("whats_new_popup_subtitle_text_color"),
    POPUP_DESCRIPTION_TEXT_COLOR("whats_new_popup_description_text_color"),
    POPUP_RATE_TEXT("whats_new_popup_rate_text"),
    POPUP_RATE_LINK("whats_new_popup_rate_link"),
    POPUP_SHARE_TEXT("whats_new_popup_share_text"),
    POPUP_SHARE_LINK("whats_new_popup_share_link"),
    POPUP_BUTTON_TEXT_COLOR("whats_new_popup_button_text_color"),
    POPUP_BUTTON_COLOR("whats_new_popup_button_color")
}