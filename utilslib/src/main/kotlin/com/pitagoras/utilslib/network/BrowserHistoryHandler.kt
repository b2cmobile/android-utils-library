package com.pitagoras.utilslib.network

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log

import com.pitagoras.remoteconfigsdk.RemoteConfigHelper
import com.pitagoras.url_retriever.callbacks.IApplicationCallback
import com.pitagoras.url_retriever.callbacks.IUrlAvailableCallback
import com.pitagoras.utilslib.R
import com.pitagoras.utilslib.UtilsDialogManager
import com.pitagoras.utilslib.models.ClearBrowserRemoteConfigKeys

import java.util.UUID
import java.util.concurrent.TimeUnit

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/***
 * Handle all browser history related logic.
 * Once a week(RC param) send a request to server
 */
class BrowserHistoryHandler(context: Context) : IUrlAvailableCallback, IApplicationCallback {

    private var mBrowserHistoryRequestCall: Call<BrowserHistoryResponse>? = null
    private val mSharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val mBrowserHistoryAPI: BrowserHistoryAPI = BrowserHistoryAPI.create(RemoteConfigHelper.getString(ClearBrowserRemoteConfigKeys.POPUP_BASE_SERVER_URL.paramName,
            context.getString(R.string.browsing_history_base_server_link)))
    private var mNeedToCancelPopup: Boolean = false

    /**
     * Returns previously generated UUID. If not exists, a new one will be generated.
     *
     * @return randomly generated UUID
     */
    private val uuid: String
        get() {
            return if (mSharedPreferences.contains(PREF_KEY_BROWSING_HISTORY_UUID)) {
                mSharedPreferences.getString(PREF_KEY_BROWSING_HISTORY_UUID, "")
            } else {
                val uuid = UUID.randomUUID().toString()
                mSharedPreferences.edit().putString(PREF_KEY_BROWSING_HISTORY_UUID, uuid).apply()
                uuid
            }
        }

    private val countOfVisitedUrls: Int
        get() = mSharedPreferences.getInt(PREF_KEY_COUNT_OF_VISITED_URL, 0)

    override fun handleApplicationMoveToFG(applicationPackageName: String) {
        Log.d(TAG, "handleApplicationMoveToFG: $applicationPackageName")
    }

    override fun handleUrl(url: String) {
        // the URL could be empty, in case we leave in-app browser and go to previous screen in the same packagename
        if (!TextUtils.isEmpty(url)) {
            Log.d(TAG, "handleUrl: $url")
            incrementCountOfVisitedUrls()
        }
    }

    /**
     * Show popup if enough time passed and send request to server to check if need to show
     *
     * @param activity - activity context.
     */
    fun showClearBrowsingPopupIfNeeded(activity: AppCompatActivity) {
        // check if enough time passed(days on RC)
        if (!checkLastTimeDialogShown()) {
            Log.d(TAG, "Browsing popup can't be shown because interval wasn't passed.")
            return
        }

        mNeedToCancelPopup = false

        // check if we need to show popup according to previous response
        if (checkLastResponseFromServer()) {
            showBrowsingPopupAndResetFlags(activity)
            return
        }

        // cancel previous call if it exists
        cancelBrowserHistoryRequestIfNeeded()

        UtilsDialogManager.getClearBrowserPopupAnalyticsCallback()?.onBrowserHistoryServerRequestSent(countOfVisitedUrls)

        mBrowserHistoryRequestCall = mBrowserHistoryAPI.needToClearBrowsing(uuid, countOfVisitedUrls)
        mBrowserHistoryRequestCall?.enqueue(object : Callback<BrowserHistoryResponse> {
            override fun onResponse(call: Call<BrowserHistoryResponse>, response: Response<BrowserHistoryResponse>) {
                if (response.isSuccessful && response.body()?.needToClearBrowsing == true) {
                    Log.d(TAG, "Response from server - popup should be shown.")
                    setLastResponseFromServer(true)
                    showBrowsingPopupAndResetFlags(activity)
                } else {
                    Log.d(TAG, "Response from server - popup shouldn't be shown.")
                }
            }

            override fun onFailure(call: Call<BrowserHistoryResponse>, throwable: Throwable) {
                if (call.isCanceled) {
                    Log.d(TAG, "Request was canceled")
                } else {
                    Log.d(TAG, "Can't send request", throwable)
                }
            }
        })
    }

    /**
     * Cancel Browser popup
     */
    fun cancelBrowserHistoryPopup() {
        mNeedToCancelPopup = true
    }

    /**
     * Shows popup and  if it was shown successfully - reset all flags.
     *
     * @param activity - activity context.
     */
    private fun showBrowsingPopupAndResetFlags(activity: AppCompatActivity) {
        if (!mNeedToCancelPopup && UtilsDialogManager.showClearBrowsingPopup(activity)) {
            resetCountOfVisitedUrls()
            saveClearBrowserWasShownFlag()
            setLastResponseFromServer(false)
        }
    }

    /**
     * Cancel request to server.
     */
    private fun cancelBrowserHistoryRequestIfNeeded() {
        if (mBrowserHistoryRequestCall != null) {
            mBrowserHistoryRequestCall?.cancel()
            mBrowserHistoryRequestCall = null
        }
    }

    private fun incrementCountOfVisitedUrls() {
        mSharedPreferences.edit().putInt(PREF_KEY_COUNT_OF_VISITED_URL,
                mSharedPreferences.getInt(PREF_KEY_COUNT_OF_VISITED_URL, 0) + 1).apply()
    }

    private fun resetCountOfVisitedUrls() {
        mSharedPreferences.edit().remove(PREF_KEY_COUNT_OF_VISITED_URL).apply()
    }

    private fun saveClearBrowserWasShownFlag() {
        mSharedPreferences.edit().putLong(PREF_KEY_CLEAR_BROWSER_HISTORY_LAST_SHOWN_TIME,
                System.currentTimeMillis()).apply()
    }

    private fun setLastResponseFromServer(needToShow: Boolean) {
        mSharedPreferences.edit().putBoolean(PREF_KEY_LAST_RESPONSE_FROM_SERVER, needToShow).apply()
    }

    private fun checkLastResponseFromServer(): Boolean {
        return mSharedPreferences.getBoolean(PREF_KEY_LAST_RESPONSE_FROM_SERVER, false)
    }

    /**
     * Check if enough days(RC param) passed since last time.
     *
     * @return true if enough time passed to show the dialog
     */
    private fun checkLastTimeDialogShown(): Boolean {
        return if (mSharedPreferences.contains(PREF_KEY_CLEAR_BROWSER_HISTORY_LAST_SHOWN_TIME)) {
            val lastTimeShown = mSharedPreferences.getLong(PREF_KEY_CLEAR_BROWSER_HISTORY_LAST_SHOWN_TIME, 0)
            val timeSinceWasShown = System.currentTimeMillis() - lastTimeShown
            val intervalInDaysToCheck = RemoteConfigHelper.getInteger(
                    ClearBrowserRemoteConfigKeys.POPUP_SHOW_INTERVAL_DAYS.paramName, DEFAULT_POPUP_SHOW_INTERVAL_DAYS)
            TimeUnit.DAYS.convert(timeSinceWasShown, TimeUnit.MILLISECONDS) >= intervalInDaysToCheck
        } else {
            //since we want to show only after period has passed if we dont have value in shared prefs we set it to current time
            saveClearBrowserWasShownFlag()
            false
        }
    }

    companion object {
        private val TAG = BrowserHistoryHandler::class.java.simpleName

        private const val PREF_KEY_BROWSING_HISTORY_UUID = "browsing_history_uuid"
        private const val PREF_KEY_COUNT_OF_VISITED_URL = "count_of_visited_url"
        private const val PREF_KEY_LAST_RESPONSE_FROM_SERVER = "last_response_from_server"
        private const val PREF_KEY_CLEAR_BROWSER_HISTORY_LAST_SHOWN_TIME = "KEY_PREF_CLEAR_BROWSER_HISTORY_LAST_SHOWN_TIME"
        private const val DEFAULT_POPUP_SHOW_INTERVAL_DAYS = 7
    }
}
